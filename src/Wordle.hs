{-# LANGUAGE ExistentialQuantification, RankNTypes, GADTs #-}
module Wordle where

import Data.Set (Set)
import qualified Data.Set as Set

data Environment = Environment {
  wordLength :: Int,
  wordList :: Set String
}

newtype Hints = Hints [(Char, Hint)]

data Hint = Correct | Contains | DoesNotContain

data AI where
  AI :: forall s. {
    internalState :: s,
    guessWord :: s -> Environment -> IO String,
    updateHints :: s -> Hints -> IO s
  } -> AI
