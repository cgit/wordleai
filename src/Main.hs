{-# LANGUAGE ExistentialQuantification, RankNTypes, GADTs #-}
module Main where

import Text.Printf
import System.Environment
import Data.Set (Set)
import qualified Data.Set as Set
import System.IO

import Wordle
import Greedy

main :: IO ()
main = do
  argv <- getArgs

  case argv of
    (word:_) -> do
      env <-
          Environment (length word) .
          Set.fromList .
          filter (\w -> length w == length word) .
          lines <$> readFile "nounlist.txt"

      if word `notElem` wordList env
        then putStrLn "Word not in wordList"
        else runMain 6 env word (greedyAI env)

    _ -> putStrLn "Need one argument"

playerAI :: AI
playerAI = AI () (\_ env -> do
  putStr $ printf "%s\r" (replicate (wordLength env) '.')
  hFlush stdout
  getLine) (\_ _ -> return ())

runMain :: Int -> Environment -> String -> AI -> IO ()
runMain round env secretWord (AI state doGuess doUpdate) = do
  if round == 0
    then gameOver
    else continue

  where

  gameOver = putStrLn "Gave Over"

  continue = do

    guess <- doGuess state env

    if not (guess `Set.member` wordList env)
      then do
        putStrLn "Word not in wordList"
        runMain round env secretWord (AI state doGuess doUpdate)

      else do
        let hints = Hints $ zipWith (\g w -> (
                g,
                if g == w
                    then Correct
                    else if g `elem` secretWord
                      then Contains
                      else DoesNotContain)) guess secretWord

        putStrLn $ printHints hints
        if guess == secretWord
          then putStrLn "Correct!"
          else do
            newState <- doUpdate state hints
            runMain (round - 1) env secretWord (AI newState doGuess doUpdate)

        where printHints (Hints h) = flip concatMap h $ \(c, h) ->
                (case h of
                   Correct -> printf "\x1b[01;7;32m%c\x1b[0m"
                   Contains -> printf "\x1b[01;7;33m%c\x1b[0m"
                   _ -> printf "%c") c
